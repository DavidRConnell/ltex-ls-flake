{ lib, stdenv, fetchurl, jre, makeWrapper }:

stdenv.mkDerivation rec {
  pname = "ltex-ls";
  version = "15.2.0";

  src = fetchurl {
    url =
      "https://github.com/valentjn/ltex-ls/releases/download/${version}/ltex-ls-${version}.tar.gz";
    sha256 =
      "1cm3c1095v6dxw8jg1n3jwm9lp7sp5vkivhjwkfd4gqs6qbc426a";
  };

  nativeBuildInputs = [ makeWrapper ];
  installPhase = ''
            runHook preInstall

            mkdir -p "$out/share/ltex-ls"
            cp -R * "$out/share/ltex-ls"

            mkdir -p "$out/bin"
            for executable in ltex-cli ltex-ls
            do
                makeWrapper "$out/share/ltex-ls/bin/$executable" \
                    "$out/bin/$executable" \
                    --prefix PATH : "${
                      lib.makeBinPath [ jre ]
                    }:$out/share/ltex-ls/bin/" \
                    --set JAVA_HOME "${jre}"
            done

            runHook postInstall
          '';

  meta = with lib; {
    description =
      "LTeX -- Grammar/Spell Checker Using LanguageTool";
    homepage = "https://valentjn.github.io/ltex/index.html";
    license = licenses.mpl20;

    maintainers = [ maintainers.offline ];
    platforms = lib.platforms.unix;
  };
}
