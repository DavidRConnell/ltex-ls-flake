{
  description = "A grammar and spell checker.";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    {
      overlay = final: prev: {
        ltex-ls =
          final.callPackage ./default.nix { jre = prev.openjdk11_headless; };
      };
    } // flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        jre = pkgs.openjdk11_headless;
      in {
        packages.ltex-ls = pkgs.callPackage ./default.nix { inherit jre; };
        defaultPackage = self.packages.${system}.ltex-ls;
        apps.ltex-cli = {
          type = "app";
          program = "${self.packages.${system}.ltex-ls}/bin/ltex-cli";
        };
        apps.ltex-ls = {
          type = "app";
          program = "${self.packages.${system}.ltex-ls}/bin/ltex-ls";
        };
      });
}
